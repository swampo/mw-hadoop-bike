1. Accendere macchine Hadoop Master e Slave
2. In Master, da terminale
    * su - hduser (password hadoop)
    * cd /usr/local/hadoop/
    * bin/start-dfs.sh
    * jps
    * bin/start-mapred.sh
    * jps
    * bin/hadoop dfs -ls /user/hduser/
    * bin/hadoop jar /home/andrea/Desktop/bike.jar it.polimi.hadoopbike.MaxTrafficStation /user/hduser/bike /user/hduser/bike-4-out
    * mkdir /tmp/out
    * bin/hadoop dfs -getmerge /user/hduser/bike-4-out /tmp/out
    * bin/stop-mapred.sh
    * bin/stop-dfs.sh
    * jps

bin/hadoop jar <path_to_JAR> <Main_QualifiedName> <input_folder> <output_folder>

bin/hadoop jar /home/andrea/Desktop/bike.jar it.polimi.hadoopbike.AvgDuration /user/hduser/bike /user/hduser/bike-1-out

bin/hadoop jar /home/andrea/Desktop/bike.jar it.polimi.hadoopbike.NumCustomers /user/hduser/bike /user/hduser/bike-2-out

bin/hadoop jar /home/andrea/Desktop/bike.jar it.polimi.hadoopbike.NumDurationAge /user/hduser/bike /user/hduser/bike-3-out

bin/hadoop jar /home/andrea/Desktop/bike.jar it.polimi.hadoopbike.MaxTrafficStation /user/hduser/bike /user/hduser/bike-4-out
