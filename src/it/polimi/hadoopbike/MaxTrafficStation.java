package it.polimi.hadoopbike;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.FileInputFormat;
import org.apache.hadoop.mapred.FileOutputFormat;
import org.apache.hadoop.mapred.JobClient;
import org.apache.hadoop.mapred.JobConf;
import org.apache.hadoop.mapred.TextInputFormat;
import org.apache.hadoop.mapred.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import it.polimi.hadoopbike.mostusedstation.*;


public class MaxTrafficStation extends Configured implements Tool {
	
	private static String intermediate = "/user/hduser/bike-4-tmp";

	@Override
	public int run(String[] args) throws Exception {		
		Configuration conf = getConf();
		Path tmpPath = new Path(intermediate);
		Path inPath = new Path(args[0]);
		Path outPath = new Path(args[1]);
		
		JobConf jobC1 = new JobConf(conf, MaxTrafficStation.class);
		jobC1.setJobName("CountTrafficStation");
        
        FileInputFormat.setInputPaths(jobC1, inPath);
        FileOutputFormat.setOutputPath(jobC1, tmpPath);

		jobC1.setMapperClass(DateStationTrafficCountMapper.class);
		jobC1.setReducerClass(DateStationTrafficCountReducer.class);
		
		jobC1.setInputFormat(TextInputFormat.class);

		jobC1.setOutputFormat(TextOutputFormat.class);
		jobC1.setOutputKeyClass(DateStationWritable.class);
		jobC1.setOutputValueClass(LongWritable.class);
		jobC1.set("mapred.textoutputformat.separator", ",");
		
		JobConf jobC2 = new JobConf(conf, MaxTrafficStation.class);
		jobC2.setJobName("MaxTrafficStationPerDate");		
		
        FileInputFormat.setInputPaths(jobC2, tmpPath);
        FileOutputFormat.setOutputPath(jobC2, outPath);	
        
		jobC2.setMapperClass(MaxTrafficStationMapper.class);
		jobC2.setReducerClass(MaxTrafficStationReducer.class);

		jobC2.setInputFormat(TextInputFormat.class);

		jobC2.setOutputFormat(TextOutputFormat.class);
		jobC2.setOutputKeyClass(Text.class);
		jobC2.setOutputValueClass(StationTrafficWritable.class);	
        
        JobClient.runJob(jobC1);
        JobClient.runJob(jobC2);
        
        return 0;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new Configuration(), new MaxTrafficStation(), args);
		System.exit(exitCode);
	}
}
