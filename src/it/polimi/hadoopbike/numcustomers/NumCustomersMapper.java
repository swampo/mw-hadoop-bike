package it.polimi.hadoopbike.numcustomers;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;


public class NumCustomersMapper extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, IntWritable> {
	private final static IntWritable one = new IntWritable(1);
	
	@Override
	public void map(LongWritable key, Text value, OutputCollector<IntWritable, IntWritable> output, Reporter reporter)	throws IOException {
		String line = value.toString();
		line = line.replaceAll("\"", "");

		//it is not a keyValue input, we have to split at every comma
		String[] parts = line.split(",");
		if (parts.length < 15 || key.get()==0) {
			return;
		}
		
		//do the job only if a customer
		if(parts[12].toLowerCase().equals("customer")) {
			//startTime is in format MM/dd/yyyy HH:mm
			String startTime = parts[1];
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			Date date = null;
			try {
				date = df.parse(startTime);
			} catch (ParseException e) {
				e.printStackTrace();
				return;
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int week = cal.get(Calendar.WEEK_OF_YEAR);
			
			//create as output <weekOfYear,1>
			output.collect(new IntWritable(week), one);	
		}
	}
}
