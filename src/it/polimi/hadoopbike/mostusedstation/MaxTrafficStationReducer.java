package it.polimi.hadoopbike.mostusedstation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class MaxTrafficStationReducer extends MapReduceBase implements Reducer<Text, StationTrafficWritable, Text, StationTrafficWritable> {
	@Override
	public void reduce(Text key, Iterator<StationTrafficWritable> values, OutputCollector<Text, StationTrafficWritable> output, Reporter reporter) throws IOException {
		long max = -1;
		List<StationTrafficWritable> items = new ArrayList<StationTrafficWritable>();

		while (values.hasNext()) {
			StationTrafficWritable stw  = values.next();
			//Copy the value into a list
			int stationId = stw.getStationId().get();
			String stationName = stw.getStationName().toString();
			long inOut = stw.getInOutCount().get();
			StationTrafficWritable newStw = new StationTrafficWritable(stationId,stationName,inOut);
			items.add(newStw);
			//Determine the max
			if(stw.getInOutCount().get()>max) {
				max = stw.getInOutCount().get();
			}
		}
		
		for(StationTrafficWritable item: items) {
			if(item.getInOutCount().get()==max) {
				//create as output <date,<stationId,stationName,maxTotalTrafficCount>>
				output.collect(key, item);		
			}
		}
	}
}
