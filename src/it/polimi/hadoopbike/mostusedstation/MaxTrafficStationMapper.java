package it.polimi.hadoopbike.mostusedstation;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

public class MaxTrafficStationMapper extends MapReduceBase implements Mapper<LongWritable, Text, Text, StationTrafficWritable> {	
	@Override
	public void map(LongWritable key, Text value, OutputCollector<Text, StationTrafficWritable> output, Reporter reporter)	throws IOException {
		String line = value.toString();
		//since the station may contain spaces, to distinguish we use " -> "
        String[] parts = line.split(",");
        
        String date = parts[0];
        int stationId = Integer.parseInt(parts[1]);
        String stationName = parts[2];
		long traffic = Long.parseLong(parts[3]);
		
		StationTrafficWritable newValue = new StationTrafficWritable(stationId,stationName,traffic);
		
		//create as output <date,<stationId,stationName,totalTrafficCount>>
		output.collect(new Text(date), newValue);
	}
}
