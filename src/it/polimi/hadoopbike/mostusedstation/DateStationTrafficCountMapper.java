package it.polimi.hadoopbike.mostusedstation;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;


public class DateStationTrafficCountMapper extends MapReduceBase implements Mapper<LongWritable, Text, DateStationWritable, LongWritable> {
	private final static LongWritable one = new LongWritable(1);
	private final static String begin = "5/31/2015 23:59";
	private final static String end = "9/1/2015 0:00";

	@Override
	public void map(LongWritable key, Text value, OutputCollector<DateStationWritable, LongWritable> output, Reporter reporter)	throws IOException {
		String line = value.toString();
		line = line.replaceAll("\"", "");

		//it is not a keyValue input, we have to split at every comma
		String[] parts = line.split(",");
		if (parts.length < 15 || key.get()==0) {
			return;
		}
				
		//startTime and stopTime is in format MM/dd/yyyy HH:mm
		String startTime = parts[1];
		String endTime = parts[2];
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat dfOut = new SimpleDateFormat("yyyyMMdd");
		Date dateStart = null;
		Date dateEnd = null;
		Date first = null;
		Date last = null;
		try {
			dateStart = df.parse(startTime);
			dateEnd = df.parse(endTime);
			first = df.parse(begin);
			last = df.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		
		String startDateKey = dfOut.format(dateStart);
		String endDateKey = dfOut.format(dateEnd);
		
		//get stations information
		int startStationId = Integer.parseInt(parts[3]);
		String startStationName = parts[4];
		int endStationId = Integer.parseInt(parts[7]);
		String endStationName = parts[8];
			
		//output: <<date,stationId,stationName>,1>
		if(dateStart.after(first) && dateStart.before(last)) {
			DateStationWritable key1 = new DateStationWritable(startDateKey,startStationId,startStationName);
			output.collect(key1, one);	
		}
		if(dateEnd.before(last) && dateEnd.after(first)) {
			DateStationWritable key2 = new DateStationWritable(endDateKey,endStationId,endStationName);
			output.collect(key2, one);
		}
	}
}
