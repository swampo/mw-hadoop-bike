package it.polimi.hadoopbike.mostusedstation;


import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;

public class DateStationTrafficCountReducer extends MapReduceBase implements Reducer<DateStationWritable, LongWritable, DateStationWritable, LongWritable> {
	@Override
	public void reduce(DateStationWritable key, Iterator<LongWritable> values, OutputCollector<DateStationWritable, LongWritable> output, Reporter reporter) throws IOException {
		long sum = 0;
		while (values.hasNext()) {
			LongWritable lw  = values.next();
			sum += lw.get();
		}
		//create as output <<date,stationId,stationName>,totalTrafficCount>
		output.collect(key, new LongWritable(sum));
	}
}
