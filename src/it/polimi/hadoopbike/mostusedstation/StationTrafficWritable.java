package it.polimi.hadoopbike.mostusedstation;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class StationTrafficWritable implements Writable {
	private IntWritable stationId;
	private Text stationName;
	private LongWritable inOutCount;

	public StationTrafficWritable() {
		this.stationId = new IntWritable();
		this.stationName = new Text();
		this.inOutCount = new LongWritable();
	}

	public StationTrafficWritable(int stationId, String stationName, long inOutCount) {
		this.stationId = new IntWritable(stationId);
		this.stationName = new Text(stationName);
		this.inOutCount = new LongWritable(inOutCount);
	}

	public IntWritable getStationId() {
		return stationId;
	}

	public void setStationId(IntWritable stationId) {
		this.stationId = stationId;
	}

	public Text getStationName() {
		return stationName;
	}

	public void setStationName(Text stationName) {
		this.stationName = stationName;
	}

	public LongWritable getInOutCount() {
		return inOutCount;
	}

	public void setInOutCount(LongWritable inOutCount) {
		this.inOutCount = inOutCount;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.stationId.write(out);
		this.stationName.write(out);
		this.inOutCount.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.stationId.readFields(in);
		this.stationName.readFields(in);
		this.inOutCount.readFields(in);
	}

	@Override
	public String toString() {
		return this.stationId + "\t" + this.stationName + "\t" + this.inOutCount;
	}
}
