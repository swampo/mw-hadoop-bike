package it.polimi.hadoopbike.mostusedstation;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class DateStationWritable implements WritableComparable<DateStationWritable> {
	private Text date;
	private IntWritable stationId;
	private Text stationName;

	public DateStationWritable() {
		this.date = new Text();
		this.stationId = new IntWritable();
		this.stationName = new Text();
	}

	public DateStationWritable(String date, int stationId, String stationName) {
		this.date = new Text(date);
		this.stationId = new IntWritable(stationId);
		this.stationName = new Text(stationName);
	}

	public Text getDate() {
		return date;
	}

	public void setDate(Text date) {
		this.date = date;
	}
	
	public IntWritable getStationId() {
		return stationId;
	}

	public void setStationId(IntWritable stationId) {
		this.stationId = stationId;
	}
	
	public Text getStationName() {
		return stationName;
	}

	public void setStationName(Text stationName) {
		this.stationName = stationName;
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.date.write(out);
		this.stationId.write(out);
		this.stationName.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.date.readFields(in);
		this.stationId.readFields(in);
		this.stationName.readFields(in);
	}

	@Override
	public String toString() {
		return this.date + "," + this.stationId + "," + this.stationName;
	}

	@Override
	public int compareTo(DateStationWritable o) {
		if (o == null)
			throw new NullPointerException();
		if (o == this)
			return 0;
		if (this.date.compareTo(o.date) > 0) {
			return 1;
		} else if (this.date.compareTo(o.date) == 0) {
			return this.stationId.compareTo(o.stationId);
		} else {
			return -1;
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof DateStationWritable))
			return false;

		DateStationWritable other = (DateStationWritable) obj;
		
		if (this.date == null) {
			if (other.date != null)
				return false;
		} else if (!this.date.equals(other.date)) {
			return false;
		}
		
		if (this.stationId == null) {
			if (other.stationId != null)
				return false;
		} else if (!this.stationId.equals(other.stationId)) {
			return false;
		}
		
		if (this.stationName == null) {
			if (other.stationName != null)
				return false;
		} else if (!this.stationName.equals(other.stationName)) {
			return false;
		}
		
		return true;
	}
}
