package it.polimi.hadoopbike.avgduration;


import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;


public class AvgDurationReducer extends MapReduceBase implements Reducer<IntWritable, FloatWritable, IntWritable, FloatWritable> {
	@Override
	public void reduce(IntWritable key, Iterator<FloatWritable> values, OutputCollector<IntWritable, FloatWritable> output, Reporter reporter) throws IOException {
		float sum = 0;
		long counter = 0;
		while (values.hasNext()) {
			counter++;
			FloatWritable fw  = values.next();
			sum += fw.get();
		}
		//create as output <weekOfYear,avgTripDuration>
		float avg = sum / counter;
		output.collect(key, new FloatWritable(avg));
	}
}
