package it.polimi.hadoopbike.avgduration;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;


public class AvgDurationMapper extends MapReduceBase implements Mapper<LongWritable, Text, IntWritable, FloatWritable> {
	@Override
	public void map(LongWritable key, Text value, OutputCollector<IntWritable, FloatWritable> output, Reporter reporter)	throws IOException {
		String line = value.toString();
		line = line.replaceAll("\"", "");
		
		//it is not a keyValue input, we have to split at every comma
		String[] parts = line.split(",");
		if (parts.length < 15 || key.get()==0) {
			return;
		}
		
		//startTime is in format MM/dd/yyyy HH:mm
		String startTime = parts[1];
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Date date = null;
		try {
			date = df.parse(startTime);
		} catch (ParseException e) {
			e.printStackTrace();
			return;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.WEEK_OF_YEAR);
		
		//get the duration which is in seconds
		int duration = Integer.parseInt(parts[0]);
		
		//create as output <weekOfYear,tripDuration>
		output.collect(new IntWritable(week), new FloatWritable(duration));
	}
}
