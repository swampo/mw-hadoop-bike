package it.polimi.hadoopbike.numdurationage;


import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.MapReduceBase;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reducer;
import org.apache.hadoop.mapred.Reporter;


public class NumDurationAgeReducer extends MapReduceBase implements Reducer<Text, NumDurationWritable, Text, NumDurationWritable> {
	@Override
	public void reduce(Text key, Iterator<NumDurationWritable> values, OutputCollector<Text, NumDurationWritable> output, Reporter reporter) throws IOException {
		long count = 0;
		float duration = 0;
		while (values.hasNext()) {
			NumDurationWritable ndw  = values.next();
			count += ndw.getNumTrips().get();
			duration += ndw.getDuration().get();
		}
		
		float avg = duration/count;
		NumDurationWritable countAndDuration = new NumDurationWritable(count,avg);
		
		//create as output <ageRange,<totalCount,avgDuration>>
		output.collect(key, countAndDuration);
	}
}
