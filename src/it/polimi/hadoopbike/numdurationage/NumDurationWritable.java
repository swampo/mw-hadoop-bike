package it.polimi.hadoopbike.numdurationage;


import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Writable;

public class NumDurationWritable implements Writable {
	private LongWritable numTrips;
	private FloatWritable duration;

	public NumDurationWritable() {
		this.numTrips = new LongWritable();
		this.duration = new FloatWritable();
	}

	public NumDurationWritable(long numTrips, float duration) {
		this.numTrips = new LongWritable(numTrips);
		this.duration = new FloatWritable(duration);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		this.numTrips.write(out);
		this.duration.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		this.numTrips.readFields(in);
		this.duration.readFields(in);
	}

	public LongWritable getNumTrips() {
		return this.numTrips;
	}

	public void setDate(LongWritable numTrips) {
		this.numTrips = numTrips;
	}

	public FloatWritable getDuration() {
		return this.duration;
	}

	public void setDuration(FloatWritable duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return this.numTrips + "\t" + this.duration;
	}
}
